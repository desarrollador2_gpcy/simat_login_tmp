package com.simat_login.controller;

import com.simat_login.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/usuarioLogin")
public class UsuarioController {
    
    	@Autowired
	UsuarioService usuarioService;
        
        @RequestMapping(value = "/status", method = RequestMethod.GET)
	public ResponseEntity<String> status() {
		return new ResponseEntity<>("OK", HttpStatus.OK);
	}
        
        @RequestMapping(value = "/validaLogin/{login}/{contrasena}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> insertTraslado(@PathVariable("login") String login, @PathVariable("contrasena") String contrasena) {
            try {                                                                   
                    String resultado = usuarioService.ValidarLogin(login, contrasena);

                    return new ResponseEntity<>(resultado, HttpStatus.OK);
                                                                                           
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
        }
}
