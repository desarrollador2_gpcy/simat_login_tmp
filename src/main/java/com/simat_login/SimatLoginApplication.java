package com.simat_login;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimatLoginApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimatLoginApplication.class, args);
	}

}
