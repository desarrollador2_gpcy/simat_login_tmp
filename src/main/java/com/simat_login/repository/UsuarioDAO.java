package com.simat_login.repository;

import com.simat_login.config.ConnectionHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.springframework.stereotype.Repository;

@Repository
public class UsuarioDAO implements IUsuarioDAO {

    @Override
    public String obtenerMD5(String PassWord) {
        String qrySelect = "SELECT CADENA_A_MD5(?) MD5 FROM DUAL";
        String Pass = null;
        try (Connection cn = ConnectionHelper.getConnection()) {            
            try (PreparedStatement sn = cn.prepareStatement(qrySelect)) {                        
                sn.setString(1, PassWord);    
                try (ResultSet rs = sn.executeQuery()) {                            
                    if (rs.next()) {
                      Pass = rs.getString("MD5");  
                    }                            
                } catch (Exception e) {  }
            } catch (Exception e) {  }
            
        } catch (Exception e) {  }  
        return Pass;
    }

    @Override
    public String obtenerPasswordBD(long Id) {
        String qrySelect = "SELECT PASSWORD FROM USUARIOS WHERE ID = ?";
        String Pass = null;
        try (Connection cn = ConnectionHelper.getConnection()) {            
            try (PreparedStatement sn = cn.prepareStatement(qrySelect)) {                        
                sn.setLong(1, Id);    
                try (ResultSet rs = sn.executeQuery()) {                            
                    if (rs.next()) {
                      Pass = rs.getString("PASSWORD");  
                    }                            
                } catch (Exception e) {  }
            } catch (Exception e) {  }
            
        } catch (Exception e) {  }  
        return Pass;
    }

    @Override
    public long obtenerIdUsuarioBD(String Login) {
        String qrySelect = "SELECT ID FROM USUARIOS WHERE LOGIN = ?";
        long usuId = 0;
        try (Connection cn = ConnectionHelper.getConnection()) {            
            try (PreparedStatement sn = cn.prepareStatement(qrySelect)) {                        
                sn.setString(1, Login);    
                try (ResultSet rs = sn.executeQuery()) {                            
                    if (rs.next()) {
                      usuId = rs.getLong("ID");  
                    }                            
                } catch (Exception e) {  }
            } catch (Exception e) {  }
            
        } catch (Exception e) {  }  
        return usuId;
    }
    
}
