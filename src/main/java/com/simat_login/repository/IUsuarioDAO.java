package com.simat_login.repository;

public interface IUsuarioDAO {
    String obtenerMD5 (String PassWord);
    String obtenerPasswordBD (long Id);
    long obtenerIdUsuarioBD (String Login);
}