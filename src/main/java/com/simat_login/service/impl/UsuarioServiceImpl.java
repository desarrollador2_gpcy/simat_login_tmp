package com.simat_login.service.impl;

import com.simat_login.repository.IUsuarioDAO;
import com.simat_login.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("UsuarioServiceImpl")
public class UsuarioServiceImpl implements UsuarioService{

    @Autowired
    private IUsuarioDAO usuarioDAO;
    
    @Override
    public String ValidarLogin(String Login, String PassWord) {
        long idUsuario = 0;
        String resultado = "";
        idUsuario = usuarioDAO.obtenerIdUsuarioBD(Login);               
        
        if (0 == idUsuario) {
            resultado = "No existe registro en la Base de Datos para el Login " + Login + " en la tabla de Usuarios";
            return resultado;
        }
        else {
            if (usuarioDAO.obtenerMD5(PassWord).equals(usuarioDAO.obtenerPasswordBD(idUsuario)) ) {
                resultado = "OK";
                return resultado;
            }
            else {
                    resultado = "La contraseña en la Base de Datos no coincide con la digitada.";
                    return resultado;
            }                
        }
    }    
}
