package com.simat_login.config;


import java.sql.Connection;
import java.util.Properties;
import java.io.InputStream;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public final class ConnectionHelper {
    private static HikariDataSource ds = null;
    private static final Object lock = new Object();
    private static Connection dummy = null;
    static final String ERROR = "Error";

    public static Connection getConnection() {
        if (ds == null) {
            synchronized (lock) {
                if (ds == null) {
                    try (InputStream input = ConnectionHelper.class.getClassLoader().getResourceAsStream("application.properties")) {
                        Properties prop = new Properties();
                        prop.load(input);
                        HikariConfig config = new HikariConfig();
                        Class.forName(prop.getProperty("spring.datasource.driver-class-name"));
                        config.setJdbcUrl(prop.getProperty("spring.datasource.url"));
                        config.setUsername(prop.getProperty("spring.datasource.username"));
                        config.setPassword(prop.getProperty("spring.datasource.password"));
                        config.addDataSourceProperty("cachePrepStmts", "true");
                        config.addDataSourceProperty("prepStmtCacheSize", "250");
                        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
                        ds = new HikariDataSource(config);
                        dummy = ds.getConnection();
                    } catch (Exception e) {
                    }
                }
            }
            return dummy;
        } else {
            try {
                return ds.getConnection();
            } catch (Exception e) {
                return dummy;
            }
        }
    }

    private ConnectionHelper() {

    }
}
